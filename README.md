# WordPress theme for Deptford TV

This is the WordPress theme for [Deptford TV](https://deptford.tv/).

It packages a static front page (the iconic Deptford TV splash page) within the
WordPress theme, so that the Deptford TV WP site can be deployed via
[Bedrock](https://roots.io/bedrock/) without any manual fiddling with "static"
files nor hacks in the web server's configuration (i.e. to serve some URIs
via WP and others as static assets).

## This is a child theme

This is a WordPress *child theme*, inheriting from the parent theme
[Adaption](https://wordpress.org/themes/adaption/) everything (templates,
styles, functions, etc.). It only adds a template for the iconic splash page.

Since this theme is meant to be managed via Bedrock, its `composer.json`
includes `wpackagist-theme/adaption` as a dependency (`require` section):
within the `composer.json` file for Bedrock on the live instance, it is
therefore enough to add this child theme in the `require` section:

```
  "require": {
    ...,
    "filmcode.org/wp-theme-deptford.tv": "^1.0"
  }
```

The parent theme will be installed automatically via Composer.

## Configuring the actual home page

Since this theme displays the static splash screen at https://deptford.tv/,
the actual home page on which users land after clicking through the
splash screen needs to be configured somehow.

WordPress only allows to set a page as "front page" (whose content can't be
displayed by the theme as the "front page" template is the actual splash screen)
and a page at whose URI the blog index will be placed
(https://deptford.tv/blog/ as per current settings).

In order to make the actual home page configurable (i.e. setting the target
for the link on the splash page), this theme provides a tiny configuration
option:

* by default, the link on the splash page will be `/start`

This is the default: if the `https://deptford.tv/start` URI is ok, then WP
editors just need to make sure that whichever page they want to appear as target
of the link on the splash page has its `slug` set to `start`.

* if WP editors want to use a different URI for the actual home page, for example
  `https://deptford.tv/about`, they will have to:
  * set the `slug` of the desired home page to whatever appropriate (e.g.
    `about`)
  * set the WP option named `dtv_splash_page_target_uri` to `/about`

This theme doesn't provide a WP admin UI to set this WP option: it needs
to be set via the `wp-cli` command line tool (e.g.
`sudo -u <bedrock_user> vendor/bin/wp option add dtv_splash_page_target_uri about`).
Once the WP option has been set for a first time, it can then be edited
from this WP dashboard page: https://deptford.tv/wp/wp-admin/options.php
(remembering to press the "Save changes" button at the very end of this page
once done).

## Updating the parent theme

If wishing to change the parent theme:

* update the appropriate dependency within this repository's `composer.json`
* make sure that all the CSS stylesheets enqueued by the parent theme are
  enqueued via the `enqueue_parent_styles()` function (within `functions.php`)
* make any other updates as required, if any
* tag the final commit with a new version number; this would be a major
  revision so the `maj` version number needs to be bumped up (e.g. from `1.*.*`
  to `2.*.*`)
* update the live site's `composer.json` with the new version of the theme (e.g.
  `"filmcode.org/wp-theme-deptford.tv": "^2.0"`) if using a `2.x` major version
* run `sudo -u <bedrock-user> composer update` to install the updated theme
  and any required dependencies (this will include the new parent theme)

## License

WordPress theme for Deptford TV
Copyright (C) 2017 Xelera

Author: andrea rota <a@xelera.eu>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
