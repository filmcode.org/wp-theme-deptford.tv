<?php
/**
 * Enqueue parent theme's stylesheet
 * Note: this assumes that the parent theme only enqueues a single stylesheet
 * named style.css. If switching to a different parent theme which enqueues
 * stylesheets other than style.css, the function below needs to be updated
 * accordingly in order to make sure that all relevant styles are available
 * when using this child theme.
 */
function enqueue_parent_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

/**
 * Decide which page to display as target of the splash page.
 * This site uses a splash page (front-page.php), which contains a link
 * to click through to the actual content.
 * In order to let site administrators change the splash page's link target,
 * the following logic is used:
 *
 * * if the 'dtv_splash_page_target_uri' wp_options value is set, this is used
 * * otherwise, /start is used
 *
 * As no admin UI is provided by this child theme to set the
 * dtv_splash_page_target_uri wp_option, this needs to be set e.g. via wp-cli.
 */
function splash_page_target_uri() {
    $target_uri = get_option('dtv_splash_page_target_uri');
    if(!empty($target_uri)) {
        // add leading slash if not present
        return (substr($target_uri, 0, 1) === '/') ? $target_uri : '/' . $target_uri;
    } else {
        return '/start';
    }
}
?>